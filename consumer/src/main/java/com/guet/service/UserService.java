package com.guet.service;

/**
 * @author MicahYin
 * @date 2020/8/29 12:00
 */
public interface UserService {
    public void buyTicket();
}
