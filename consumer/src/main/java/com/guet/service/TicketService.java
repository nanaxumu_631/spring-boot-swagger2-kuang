package com.guet.service;

/**
 * @author MicahYin
 * @date 2020/8/29 11:25
 * 用于整合zookeeper和dubbo的测试服务，提供买票功能
 */
public interface TicketService {
    public String getTicket();
}
