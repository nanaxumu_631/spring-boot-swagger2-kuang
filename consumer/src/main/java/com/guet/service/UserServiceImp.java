package com.guet.service;


import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

/**
 * @author MicahYin
 * @date 2020/8/29 20:53
 */
@Service
public class UserServiceImp implements UserService{
    //想拿到provider-server提供的票，要去注册中心拿到服务
    @Reference   //1.引用，2.pom坐标  3.可以定义路径相同的接口名
    TicketService ticketService;
    @Override
    public void buyTicket() {
        System.out.println(ticketService.getTicket());
    }
}
