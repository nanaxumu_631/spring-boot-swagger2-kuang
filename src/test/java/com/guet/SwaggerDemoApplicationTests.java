package com.guet;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@SpringBootTest
class SwaggerDemoApplicationTests {
	@Autowired
	JavaMailSenderImpl mailSender;

	/**
	 *发送简单邮件
	 */
	@Test
	void contextLoads() throws MessagingException{
		SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
		simpleMailMessage.setSubject("你好啊micah");
		simpleMailMessage.setText("have a good day");

		simpleMailMessage.setTo("yinwq887@163.com");
		simpleMailMessage.setFrom("1456719640@qq.com");
		mailSender.send(simpleMailMessage);
	}

	/**
	 * 发送复杂邮件
	 */
	@Test
	void contextLoads2() throws MessagingException{
		//一个复杂的邮件
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		//组装
		MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true);
		mimeMessageHelper.setSubject("你好");
		mimeMessageHelper.setText("<p style='color:red'>这是一个邮件测试文本....</p>",true);
		//附件
		mimeMessageHelper.addAttachment("1.jpg",new File("C:/Users/Micah/Pictures/156242031289.jpg"));

		mimeMessageHelper.setTo("yinwq887@163.com");
		mimeMessageHelper.setFrom("1456719640@qq.com");

		mailSender.send(mimeMessage);
	}
}
