package com.guet.service;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

/**
 * @author MicahYin
 * @date 2020/8/29 11:27
 */
@Service  //此时也需要导入service，只不过导入的是dubbo下的service
@Component  //使用了Dubbo后尽量不要用Service注解，因为Dubbo也有和spring同样的Service注解，但是只有导入了Dubbo的Service注解才能使用Dubbo
public class TicketServiceImp implements TicketService{
    @Override
    public String getTicket() {
        return "Micah,today is a nice day!";
    }
}