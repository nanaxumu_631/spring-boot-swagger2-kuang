package com.guet.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author MicahYin
 * @date 2020/8/27 12:00
 * 异步任务-->比如发送邮件....
 * 完成异步任务的示例代码，
 */
@Service
public class AsyncService {
    /**
     * 告诉Spring这是一个异步的方法,随后需要在main主类中使用@EnableAsync开启异步功能
     */
    @Async
    public void async(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("数据正在处理.......");
    }
}
