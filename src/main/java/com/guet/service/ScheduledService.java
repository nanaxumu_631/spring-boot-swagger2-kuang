package com.guet.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * @author MicahYin
 * @date 2020/8/27 17:10
 * 用于执行定时任务，在某个特定的时间执行
 */
@Service
public class ScheduledService {
    /**
     * 注意：如果在表达式中出现中文符号，则会报错
     * 30 15 10 * * ？           每天10点15分30秒执行
     * 30 0/5 10,18 * * ？       每天10点和18点，每隔5分钟执行一次
     * 0 15 10 ? * 1-6           每个月的周一到周六10.15分执行一次
     */


    //秒 分 时 日 月 周几
    @Scheduled(cron = "0 22 17 * * ?")
    public void hello(){
        System.out.println("定时任务被执行了.....");
    }
}
