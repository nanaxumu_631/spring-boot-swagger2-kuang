package com.guet.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @author MicahYin
 * @date 2020/8/26 15:09
 */

/**
 * 可以通过配置实现生产环境和发布环境的区分
 * @ConditionalOnProperty  可以通过注解的方式，读取配置文件中的值，控制当前配置类是否生效
 * 也可以使用@Profile注解来区分生产环境和测试环境
 */
@Configuration
@EnableSwagger2   //开启swagger2
public class SwaggerConfig {
    @Value("${swagger.enable}")
    private Boolean swagger;
    /**
     *可以通过配置多个docket的方式，通过@Bean注解，将其加入到spring容器中，
     * 这样的是不同的开发组可以配置自己的swagger
     * 可以通过  .groupName()  方法来设置组的名称
     */
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2 )
                .apiInfo(apiInfo())
//                .groupName("A")
                /**
                 * 通过配置swagger2是否开启，可以通过配置文件，只在项目开发版本中开启，而在发布版本中不开启
                 */
                .enable(swagger)
                .select()
                /**
                 * RequestHandlerSelectors,配置需要扫描接口的方式
                 * basePackage:指定要扫描的包
                 * any()：扫描全部
                 * none():不扫描
                 * withClassAnnotation:扫描类上的注解，参数是一个注解的反射对象
                 * withMethodAnnotation:扫描方法上的注解
                 */
                .apis(RequestHandlerSelectors.basePackage("com.guet.Controller"))
                /**
                 * path()，过滤什么路径,比如设置为"/guet/**"
                 * 那么只有请求路径包含/guet/的才能被扫描
                 */
                //.paths(PathSelectors.ant("/guet/**"))
                .build();
    }
    private ApiInfo apiInfo(){
        Contact DEFAULT_CONTACT = new Contact("Micah", "https://www.guet.edu.cn", "yinwq887@163.com");
        return new ApiInfo(
                "guet",
                "正德厚学",
                "1.0",
                "urn:tos",
                DEFAULT_CONTACT,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<VendorExtension>());
    }
}
