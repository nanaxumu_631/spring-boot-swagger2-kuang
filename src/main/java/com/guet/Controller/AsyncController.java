package com.guet.Controller;

import com.guet.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author MicahYin
 * @date 2020/8/27 12:02
 */
@RestController
public class AsyncController {
    @Autowired
    AsyncService asyncService;

    @RequestMapping("/async")
    public String async(){
        asyncService.async();
        return "OK";
    }
}
