package com.guet.Controller;

import com.guet.pojo.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author MicahYin
 * @date 2020/8/26 11:03
 */
@Controller
public class HelloController {
    @GetMapping("/")
    @ResponseBody
    public String hello(){
        return "hello";
    }

    /**
     *只要我们的接口中，返回值存在实体类，他就会被扫描到swagger2中
     */
    @PostMapping("/user")
    public User user(){
        return new User();
    }

    @ApiOperation("Hello控制类")
    @PostMapping("/hello")
    @ResponseBody
    public String hello2(@ApiParam("用户名") String username){
        return "hello"+username;
    }
}
