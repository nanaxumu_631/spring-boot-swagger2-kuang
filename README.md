# springBoot-swagger2-kuang

#### 介绍
狂神swagger2教程代码
#### 课程b站地址
https://www.bilibili.com/video/BV1PE411i7CV?t=3&p=47
## 学习笔记 zookeeper和dubbo
### zookeeper
- ZooKeeper是一个分布式的，开放源码的分布式应用程序协调服务，是一个注册中心
是Google的Chubby一个开源的实现，是Hadoop和Hbase的重要组件。
- 可以直接在apache网站上选择国源，下载zookeeper
- 下载完成之后进行解压，随后在conf文件夹下复制zoo_sample.cfg文件为zoo.cfg，
在zoo.cfg文件中，可以对zookeeper进行个性化配置
- 在bin文件夹下点击zkServer.cmd即可开启zookeeper
- 点击zkCli.cmd即可开启客户端对zookeeper进行测试

### dubbo
- dubbo-admin：是一个监控管理后台，可以查看我们注册了哪些服务，哪些服务被消费了....
- 在github上下载dubbo的源码：https://github.com/apache/dubbo-admin/tree/master
- 在解压目录下的，dubbo-admin-master/dubbo-admin/src/main/resources/application.properties中，可以修改zookeeper的地址
- 在目录下，用管理员权限执行cmd，使用mvn clean package -Dmaven.test.skip=true (跳过测试)
- 打包完成之后，在dubbo-admin-master\dubbo-admin\target目录下生成的jar文件，就是我们需要的文件，
可以通过java -jar的方式执行此文件

## zookeeper和dubbo使用步骤
前提:zookeeper服务已开启
1. 提供者提供服务
    1. 导入依赖
    2. 配置注册中心的地址，以及服务发现名，和要扫描的包
    3. 在想要被注册的服务上增加一个注解@Service（这个注解和spring的不是用一个！！）
2. 消费者消费服务
    1. 导入依赖
    2. 配置注册中心地址，配置自己的服务名
    3. 从远程注入服务  @Reference